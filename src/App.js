import React, {useEffect} from "react";
// import { useHistory } from 'react-router-dom';

import "./App.css";
//components
import Routers from "./components/pages-route";

//NESSA PÁGINA, VAMOS APENAS ADICIONAS O COMPONENTE ROTAS, OS 'LINK TO' E O ESTILO PELO ANTD

// const userChoice = JSON.parse(localStorage.getItem('user'));

function App() {
  // const history = useHistory();
  
  // useEffect(() => {
  //   userChoice !== null ? history.push('/timeline') : history.push('/') 
  // },[userChoice])

  return (
    <div className="App">
      <Routers />
    </div>
  );
}

export default App;
